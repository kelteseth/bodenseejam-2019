# Scramble
# Copyright (C) 2018  ScrambleSim and contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

extends Node
var peer
const ENTITIES_PATH = "/root/Scramble/World/Entities"


func _ready():
    get_tree().connect("network_peer_connected", self, "_client_connected")
    get_tree().connect("network_peer_disconnected", self, "_client_disconnected")
    get_tree().connect("connected_to_server", self, "_connected_ok")
    get_tree().connect("connection_failed", self, "_connected_fail")
    get_tree().connect("server_disconnected", self, "_server_disconnected")
    peer =  WebSocketClient.new()
    
    # Setup accelerometer input
    JavaScript.eval("""
    let value;

    var x;
    var y;
    var z;

    var tiltX;
    var tiltY;

    function accelerometerUpdate(event) {
        x = event.accelerationIncludingGravity.x*1;
        y = event.accelerationIncludingGravity.y*1;
        z = event.accelerationIncludingGravity.z*1;
        tiltX = Math.atan2(y, z);
        tiltY = Math.atan2(x, z);
    }

    window.addEventListener("devicemotion", accelerometerUpdate, true);
    """, true)
    

func _process(delta):
    var x = 0
    var y = 0
    var z = 0
    
    var tiltX = 0
    var tiltY = 0
    
    var steering_angle = 0
    
    # Mobile users
    if JavaScript.eval("!(window.DeviceMotionEvent == undefined)"):
        # Get steering angle
        x = JavaScript.eval("x;", true)
        y = JavaScript.eval("y;", true)
        z = JavaScript.eval("z;", true)
    
        tiltX = JavaScript.eval("tiltX;", true)
        tiltY = JavaScript.eval("tiltY;", true)
        
        if tiltX:
            steering_angle = tiltX / 1.5; # -1.0 to 0.0 to 1.0

    # Keyboard users
    if Input.is_action_pressed("ui_right"):
        steering_angle = steering_angle + 1
    if Input.is_action_pressed("ui_left"):
        steering_angle = steering_angle - 1

    # Update multiplayer events, if any arrived
    if (peer.get_connection_status() == NetworkedMultiplayerPeer.CONNECTION_CONNECTED || peer.get_connection_status() == NetworkedMultiplayerPeer.CONNECTION_CONNECTING):
        rpc_unreliable_id(1, 'update_steering_angle', steering_angle)   
        peer.poll();



func _client_connected(id):
    if id == 1:
        return  # ignore connect event for self



func _client_disconnected(id):
    Global.log("Other Player (id: " + str(id) + ") disconnected from server")


# Called when connecting worked (called after network_peer_connected arrives for self)
func _connected_ok():
    Global.log("Successfully connected to server!")
    Global.log("Unique ID of this client: " + str(get_tree().get_network_unique_id()))

    var playername = get_tree().get_root().get_node("Scramble/UI/Container/HBoxContainer/VBoxContainer2/Name").text;
    rpc_id(1, 'player_name_textChanged',playername)
    Global.log("Conneceted %s" %playername);

func _connected_fail():
    Global.log("Connect to server failed!")


func _server_disconnected():
    Global.log("Server disconnected")

# =============== UI ===============

func _on_Button_Connect_button_down2():
    var url = get_node('/root/Scramble/UI/Container/HBoxContainer/VBoxContainer2/IP').text
    url = "ws://%s" % url 
    var error = peer.connect_to_url(url, PoolStringArray(), true);
    get_tree().set_network_peer(peer)


func _on_Accelerate_button_down():
    rpc_id(1, 'accelerate', true)


func _on_Break_button_down():
    rpc_id(1, 'decelerate', true)


func _on_Accelerate_button_up():
    rpc_id(1, 'accelerate', false)


func _on_Break_button_up():
    rpc_id(1, 'decelerate', false)


func _on_Button_Reset_button_down():
    rpc_id(1, 'reset_car')
