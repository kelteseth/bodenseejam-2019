extends LineEdit

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
    var url = JavaScript.eval('window.location.href.split("/")[2].split(":")[0];')
    self.text = "%s:5000" % url


func _on_IP_focus_entered():
    var test = JavaScript.eval('prompt("%s", "%s");' % ["Enter the server ip and port", self.text], true)
    self.text = test
