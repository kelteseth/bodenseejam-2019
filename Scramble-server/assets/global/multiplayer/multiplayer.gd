# Scramble
# Copyright (C) 2018  ScrambleSim and contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

extends Node

onready var car = preload("res://assets/entities/car/car_base.tscn")

const ENTITIES_PATH = "/root/Scramble/World/Entities"

const PORT = 5000
const MAX_PLAYER_COUNT = 200
var peer
var cars

master func reset_car():
    var player_id = get_tree().get_rpc_sender_id()
    var new_pos = get_node("/root/Scramble/World/Spawns").get_spawn()
    get_node("/root/Scramble/World/%s/BODY" % str(player_id)).respawn()

master func player_name_textChanged(var name):
    var player_id = get_tree().get_rpc_sender_id()

    for car in cars:
       Global.log("%s" %car.name)
       if car.name ==  String(player_id):
           car.get_node("BODY").setCarName(name)

master func update_steering_angle(steering_angle):
    var client_id = str(get_tree().get_rpc_sender_id())
    Global.client_inputs[client_id].steering_angle = steering_angle

master func accelerate(pressed):
    var client_id = str(get_tree().get_rpc_sender_id())
    Global.client_inputs[client_id].forward = pressed

master func decelerate(pressed):
    var client_id = str(get_tree().get_rpc_sender_id())
    Global.client_inputs[client_id].backward = pressed


func _ready():
    Global.log("Starting server")
    cars = []

    # Event setup
    get_tree().connect("network_peer_connected", self, "_client_connected")
    get_tree().connect("network_peer_disconnected", self, "_client_disconnected")


    peer = WebSocketServer.new();
    peer.listen(PORT, PoolStringArray(), true);
    get_tree().set_network_peer(peer)

    Global.log("Server started, listening on port %s" % PORT)

func _process(delta):
    if peer.is_listening(): # is_listening is true when the server is active and listening
        peer.poll();

func _client_connected(new_id):
    Global.log('Client ' + str(new_id) + ' connected to Server')
    Global.player_ids.append(new_id)

    Global.client_inputs[str(new_id)] = {
        "steering_angle": 0.0,
        "forward": false,
        "backward": false,
    }
    
    var spawn_pos = get_node("../World/Spawns").get_spawn()

    var car_instance = car.instance()
    car_instance.name = str(new_id)
    cars.push_back(car_instance)
    car_instance.transform = spawn_pos
    get_tree().get_root().get_node('Scramble/World').add_child(car_instance)


# Called if a player closes a game gracefully
# Clients also time out if not gracefully disconnecting
func _client_disconnected(id):
    Global.log('Client ' + str(id) + ' disconnected from Server')

    Global.player_ids.erase(id)

    get_node("/root/Scramble/World/%s" % str(id)).queue_free()
