extends Area

func _on_Area_body_entered(body):
    if body.name == "BODY":
        var point = get_node("../Position").global_transform
        body.set_respawn(point)