extends Spatial

var spawns = []
var index = 0

func _ready():
    self.spawns = self.get_children()

func get_spawn():
    self.index += 1
    
    if self.index >= self.get_child_count():
        self.index = 1
    
    return self.spawns[index-1].global_transform